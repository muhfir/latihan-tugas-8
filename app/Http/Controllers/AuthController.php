<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar(){
        return view('halaman.register');
    }

    public function kirim(Request $request){
       //dd($request->all());
       $nama  = $request['firstname'];
       $nama2 = $request['lastname'];

       return view('halaman.coming', compact("nama", "nama2"));
    }
}

