<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\cast;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.create');
    }

    public function posting(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]);  
        
        $cast = new cast;
 
        $cast->nama = $request->nama;
        $cast->umur = $request->umur;
        $cast->bio = $request->bio;
 
        $cast->save();

        return redirect('/cast');
    }

    public function index()
    {
        $cast = cast::all();
        return view('cast.index', compact('cast'));
    }
    
    public function show($cast_id)
    {
        $cast = cast::where('id', $cast_id)->first();
        return view('cast.show', compact('cast'));
    }
    public function edit($cast_id)
    {
        $cast = cast::where('id', $cast_id)->first();
        return view('cast.edit', compact('cast'));
    }
    public function apdet(Request $request, $cast_id)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ]); 
        $cast = cast::find($cast_id);
 
        $cast->nama = $request ['nama'];
        $cast->umur = $request ['umur'];
        $cast->bio = $request ['bio'];         
        
        $cast->save(); 

        return redirect('/cast');
    }
    public function destroy($cast_id)
    {
        $cast = cast::find($cast_id);
 
        $cast->delete();
        return redirect('/cast');
    }
}
