@extends('layout.master')
@section('title')
Halaman Pendaftaran
@endsection

@section('content')
<h2>Buat Akun Baru</h2>
<form action="/welcome" method="post">
   
<h4>Sign Up Confirm</h4>
    <form action="">
        @csrf
        <label for="text">First Name:</label> <br> <br>
        <input type="text" name= "firstname"> <br> <br>
        <label for="text">Last Name:</label> <br> <br>
        <input type="text" name = "lastname"> <br> <br>
        <label for="Gender">Gender</label> <br> <br>
        <input type="radio" name="gen"> Male <br>
        <input type="radio" name="gen"> Female <br>
        <input type="radio" name="gen"> Other <br><br> 
        <label for="text">Nationality</label> <br> <br>
        <select name="wn" id="">
            <option value="1">Indonesia</option>
            <option value="2">Foreign</option>
        </select> <br> <br>
        <label for="lang">Language Spoken</label> <br> <br>
        <input type="checkbox" name="lang">Bahasa Indonesia <br>
        <input type="checkbox" name="lang">English <br>
        <input type="checkbox" name="lang">Other <br> <br>
        <label for="bio">Bio</label> <br>
        <textarea name="bio" cols="30" rows="10"></textarea> <br>
        <input type="submit" value="Sign Up">
    </form>

@endsection