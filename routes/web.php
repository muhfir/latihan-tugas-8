<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@home');

Route::get('/form', 'AuthController@daftar');

Route::post('/welcome', 'AuthController@kirim');

Route::get('/data-table', 'IndexController@table');

//CRUD CAST
//Create
Route::get('/cast/create', 'CastController@create');
Route::post('/cast', 'CastController@posting');

//Read
Route::get('/cast', 'CastController@index');
Route::get('/cast/{cast_id}', 'CastController@show');

//Update
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
Route::put('/cast/{cast_id}', 'CastController@apdet');

//Delete
Route::delete('cast/{cast_id}', 'CastController@destroy');
